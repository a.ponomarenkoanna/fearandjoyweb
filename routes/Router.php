<?php

/* This function coordinates content of return page according to user status:
/* is user authorised(authorised,id)
/* user privileges(role) 
/* is he taking test(test, content)
/* is he watching additional information(content)
*/
class Router{
    private $authorised;
    private $content;
    private $role_id;
    private $action;
    private $test;
    private $id;

    function __construct($authorised,$content, $role_id,$action,$test,$id)
    {
        $this->authorised = $authorised;
        $this->content = $content; 
        $this->role_id = $role_id;
        $this->action = $action;
        $this->test = $test;
        $this->id = $id;
    }

public function getRoute(){
    $authorised = $this->authorised;
    $role_id = $this->role_id;
    $action = $this->action;
    $id = $this->id;
    /*For unauthorised user available actions:
    /*  watch map with labels (default)
    /*  goto WorldStatistics
    /*  see test list
    /*  login/registration
    */
    if (!$this->authorised)
    {
        if ($this->content){
            if (($this->content == "World statistics") || ($this->content =="chooseTest")){
                include "view/$this->content.php";
                return;  
            } else {
                $action = "notAvailable";
            }
        }
        
        if ($this->action){
            if ($this->unauthorisedAction($this->action)){
                $this->getRoute();
            } else {
                $action = $this->action;
                if ($action == "resetPassword" || $action == "confirmed")
                    return;
            }         
        }
        include 'view/map.php';

    } else {
    /*For authorised user available actions:
    /*  watch map with labels (default)
    /*  goto profile options
    /*  see test list
    /*  take tests and push results
    /*  logout
    */    
        if ($this->action){
            if ($this->authorisedAction($this->action)){
                $this->getRoute();
            } else {
                return;
            }          
        }
        
        if ($this->content){
            if ($this->authorisedContent()){
                $action = $this->action;
            } else {
                return;
            }
        }

        include 'view/map.php';
    }
}

/**
 * "unauthorisedAction" is part of routing for unauthorised users
 * It includes routing for actions with authorisation/registration/password resetting
 * It returns true if variables are changed
 */
private function unauthorisedAction(){
    switch($this->action){
        case("Login"): 
            $this->login();
            return true;
        case("Signup"):
            $this->signup();                 
            return true;
        case("confirmed"):
            include 'view/confirmed.php';
            return false;
        case("ForgotPassword"):
            $this->forgotPassword();
            return true;
        case("resetPassword"):
            include 'view/resetPassword.php';
            return false;
        case("passwordReseted"):
            if ($this->resettedPassword()){
                $this->action = "resetted-password";
            } else {
                $this->action = "not-resetted-password";
            }
            return false;    
        default:
            return false;
        }
}

/**
 * "authorisedAction" is part of routing for authorised users
 * It includes routing for actions with authorisation(logout)
 * The function returns true if variables are modified.
 */
private function authorisedAction(){
    if ($this->action == "Logout"){
        $this->logout();
        return true;
    }
}

/**
 * "authorisedContent" is part of routing for taking tests 
 * and additional information for authorised users
 * It includes routing for taking test
 * It returns true if variables are changed
 */
private function authorisedContent(){
    $authorised = $this->authorised;
    $role_id = $this->role_id;
    $id = $this->id;
    if ($this->test > 0){    
        $test = $this->test;
        if ($this->content == "submitResult"){
                if ($this->submitResult()){
                    $this->action="resultSubmitted";
                }else{
                    $this->action="resultNotSubmitted";
                } 
                return true;
        } else {
            include "view/$this->content.php";
            return false;
        }
    }
    else {
        include "view/$this->content.php";
        return false;
    }
}

/**
 * function "login" is authorasing user. The function:
 *      checks wether user exists
 *      creates session
 *      sets session cookie
 */
private function login(){
    $mail = $_POST['email']; 
    $password = $_POST['password'];
    $timeAlive = 50000;

    $conn = new Authorisation;
    if ($ret = $conn->authorise($mail, $password)){
        $session_key = $conn->startSession($ret['id'],$ret['role_id']);                      
        setcookie ("session_key", $session_key, time() + $timeAlive);                  
        $this->authorised = true;

        $session_info = $conn->getSessionInfo($session_key);
        $this->role_id = $session_info[0]['role_id'];
        $this->id = $session_info[0]['user_id'];
        $this->action = false;    
    } else {
        $this->action = "wrong-login";
    }
}

/**
 * function "signup" is for creating new user. The function:
 *      checks wether user already exists
 *      creates user
 *      sends email for confirmation profile
 */
private function signup(){

    $email = $_POST['email'];
    $password = $_POST['password'];
    $name =$_POST['name'];
    $surname = isset($_POST['surname'])? $_POST['surname']:"";
    $mailhash = password_hash($email.$name, PASSWORD_BCRYPT);

    $conn = new Registration;
    $res = $conn->registry($name,$surname,$email,$password);

    if ($res){

        //sending confirmstion to email
        $message = '
        <html>
        <head>
        <title>Confirm your profile</title>
        </head>
        <body>
        <h3>Thank you for registrating at Fear&Joy</h3>
        <p>To confirm follow the <a href="http://fearandjoy.com?action=confirmed&hash='. $mailhash .'&email='.$email.'">link</a></p>
        </body>
        </html>';

        $subject = "Confirm your profile Fear&Joy";
        $headerss = "MIME-Version: 1.0\r\n";
        $headerss.="Content-type: text/html; charset=utf-8\r\n";
        $headerss.="Date: ".date('D, d M Y h:i:s')."\r\n";
        $headerss.="From: <no-reply@fear&joy>\r\n";

        if (!empty($email) && !empty($subject) && !empty($message)) {
            $result = mail($email, $subject, $message,$headerss);
        }	

        if ($result) {
            $this->action = "signuped";
        }
    } else {
        $this->action = "mail-occupied";
    }
}

/**
 * function "logout" is unauthorasing user. The function:
 *      finishes session
 *      unsets session cookie
 *      changes current settings
 */
private function logout(){
    $conn = new Authorisation;
    $conn->finishSession($_COOKIE["session_key"]);

    setcookie ("session_key", "", time() - 50000);
    $this->authorised = false;  
    $this->id = false;
    $this->role_id = 0;
    $this->action = false;
}

/**
 * function "forgotPassword" is for making requst to change password for unauthorased user. The function:
 *      sends mail with link for changing password
 */
private function forgotPassword(){
    $conn = new Registration;

    $mail = $_POST['email'];
    $name = $conn->getNameSurname($mail);;
    $mailhash = password_hash($mail.$name['name'].$name['surname'], PASSWORD_BCRYPT);

    //sending confirmstion to email
    $message = '
    <html>
    <head>
    <title>Reset</title>
    </head>
    <body>
    <h3>Somebody wanted to change your password at Fear&Joy</h3>
    <p>If you did not perform the request, please ignore. </p>
    <p>To reset follow the <a href="http://fearandjoy.com?action=resetPassword&hash='. $mailhash .'&email='.$mail.'">link</a></p>
    </body>
    </html>';

    $subject = "Resetting password Fear&Joy";
    $headerss = "MIME-Version: 1.0\r\n";
    $headerss.="Content-type: text/html; charset=utf-8\r\n";
    $headerss.="Date: ".date('D, d M Y h:i:s')."\r\n";
    $headerss.="From: <no-reply@fear&joy>\r\n";

    if (!empty($mail) && !empty($subject) && !empty($message)) {
        if (mail($mail, $subject, $message,$headerss)) {
            $this->action = "forgot-sent";
        }
    }	
}

/**
 * function "submit" is for for publication result of test. The function:
 *      adds result to db
 */
private function submitResult(){
    $conn = new Tests;
    $result = $_POST['result'];
    $lat = explode(',',$_POST['placemarkpos'])[0];
    $lng = explode(',',$_POST['placemarkpos'])[1];
    if ($conn->addResult($this->id,$this->test,$lat,$lng,$result)){
        return false;
    } else {
        return true;
    }
}

/**
 * function "submit" is for for publication result of test. The function:
 *      adds result to db
 */
private function resettedPassword(){
    $conn = new Reset;
    $mail = $_POST['mail'];
    $password = $_POST['password'];
    
    if ($conn->resetPassword($mail,$password)){
        return true;
    } else {
        return false;
    }
}

}
?>