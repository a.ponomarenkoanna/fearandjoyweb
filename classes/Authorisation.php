<?php
/*
This class provides handlers and information providers for log in(authorisation):
    /* checking info for authorisation(mail, password)
    /* start/finish session 

*/
class Authorisation extends Connection{
    public function authorise($mail, $password){
        $queryResult = $this->getInformation("select id, mail, password,role_id from user_ where mail = \"$mail\"");
        if( count($queryResult) > 0) {
            $hash = $queryResult[0]["password"];
            if(password_verify($password, $hash)){
                $ret = [
                    'role_id' => $queryResult[0]["role_id"],
                    'id' => $queryResult[0]["id"] ];
                return  $ret;
            } else
                return false;
        } else {
            return false;
        }
    }

    public function startSession($user_id,$role_id){
        try{ 
            $date = explode('+',date('c'))[0];
            $hash = password_hash($user_id.$date, PASSWORD_BCRYPT);
            $str = "insert into session_ (session_key,user_id,role_id,date_) values
            (\"$hash\",$user_id,$role_id,\"$date\" )";
            $queryResult = $this->getInformation($str);
                return $hash;
            } catch (Exception $ex){
                return $ex->getMessage();
            }
    }

    public function finishSession($session_key){
        try{
            if ($this->getInformation("select * from session_ where session_key= \"$session_key\"")){

                $str = "delete from session_ where session_key= \"$session_key\"";
                $queryResult = $this->getInformation($str);
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex){
            return $ex->getMessage();
        }
    }

    public function getSessionInfo($session_key){
        $queryResult = $this->getInformation("select * from session_ where session_key= \"$session_key\"");
        //+"where tr.date_ >= datefrom and tr.date_ <= dateto
        return $queryResult;
    }
   
}