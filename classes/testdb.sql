--CREATION--

alter table question add column mode_ int;
alter table question add constraint foreign key (mode_) references test_mode(id) on delete cascade;
--creating database
create database fearandjoy;
use fearandjoy;

--table test mode is showing method of calculating result
create table test_mode(
	id int primary key auto_increment,
    name varchar(100)
);
--table test is for global infomation about tests
create table test(
    id int primary key auto_increment,
    name varchar(100),
    description varchar(500),
    mode_  int,
foreign key(mode_) references test_mode(id) on delete cascade
);

--table question is for all questions for each test
create table question(
    id int not null primary key auto_increment,
    test_id int not null,
    question varchar(150),
    mode_ int DEFAULT '1',
foreign key(test_id) references test(id) on delete cascade,
foreign key(mode_) references test_mode(id) on delete cascade
);

--table answer is for all possible answers and options
create table answer(
    id int not null primary key auto_increment,
    question_id int not null,
    answer varchar(100),
    weight int,
foreign key(question_id) references question(id) on delete cascade    
);

--table option is for posiible activities(use-cases) that is provided
create table option_(
    id int not null primary key auto_increment,
    name varchar(20),
    description varchar(150) 	
);

--table role sets all the possible user roles that provides options
create table role(
    id int not null primary key auto_increment,
    name varchar(50)
);

--table role_option sets possible options to roles
create table role_option(
    id int not null primary key auto_increment,
    role_id int,
    option_id int,
foreign key(role_id) references role(id) on delete cascade,
foreign key(option_id) references option_(id) on delete cascade
);

--table user is for informations about user
create table user_(
    id int not null primary key auto_increment,
    name varchar(50),
    surname varchar(50),
    mail varchar(50),
    password binary(60) unique,
    image varchar(50),
    role_id int default 1,
    confirmed boolean default false, 
foreign key(role_id) references role(id) on delete cascade
);

--table tast result is for information about test results and labels
create table test_result(
    id int not null primary key auto_increment,
    user_id int,
    test_id int,
    lat decimal(10,7),
    lng decimal(10,7),
    date_ datetime default current_timestamp, 
    result int default null,
constraint chk_res check (result<=100 and result>=0),
foreign key(test_id) references test(id) on delete cascade,
foreign key(user_id) references user_(id) on delete cascade
);

create table session_ (
    session_key varchar(60),
	user_id int,
    role_id int,
    date_ timestamp,
foreign key(user_id) references user_(id) on delete cascade
);
--DEFAULT ROLES INSERTION--
insert into test_mode(name) values 
('sum'),
('max');

insert into role (id, name) values 
(1,'user'),
(2,'moderator'),
(3,'admin');

insert into option_(id,name, description) values
(1,'My profile','See and edit profile data'),
(2,'My history','See all my results and statistics'),
(3,'My labels','See all my results on map'),
(4,'World statistics','See world statistics'),
(5,'To users','See and edit user list'),
(6,'To tests','See and edit test list'),
(7,'To test results','See and edit test results'),
(8,'To summury method','See and edit summurising function');

insert into role_option (role_id,option_id) values 
(1,1),
(1,2),
(1,3),
(1,4),
(2,1),
(2,2),
(2,3),
(2,4),
(2,6),
(2,8),
(3,1),
(3,2),
(3,3),
(3,4),
(3,5),
(3,6),
(3,7),
(3,8);
--TEST INSERTION--
insert into test(name, description) values 
('test1','test1 description'),
('test2','test2 decription'),
('test3','test3 decription'),
('test4','test4 decription');

insert into question(test_id, question) values 
(1,'question 1 for test 1'),
(1,'question 2 for test 1'),
(1,'question 3 for test 1'),
(1,'question 4 for test 1'),
(1,'question 5 for test 1'),
(1,'question 6 for test 1'),
(2,'question 1 for test 2'),
(2,'question 2 for test 2'),
(2,'question 3 for test 2'),
(2,'question 4 for test 2'),
(3,'question 1 for test 3'),
(3,'question 2 for test 3'),
(3,'question 3 for test 3'),
(3,'question 4 for test 3'),
(4,'question 1 for test 4'),
(4,'question 2 for test 4'),
(4,'question 3 for test 4');


insert into answer (question_id, answer, weight) values 
(1,'answer 1',1),
(1,'answer 2',1),
(1,'answer 3',1),
(1,'answer 4',1),
(1,'answer 5',1),
(2,'answer 1',1),
(2,'answer 2',1),
(2,'answer 3',1),
(2,'answer 4',1),
(2,'answer 5',1),
(3,'answer 1',1),
(3,'answer 2',1),
(3,'answer 3',1),
(3,'answer 4',1),
(3,'answer 5',1),
(4,'answer 1',1),
(4,'answer 2',1),
(4,'answer 3',1),
(5,'answer 1',1),
(5,'answer 2',1),
(6,'answer 1',1),
(6,'answer 2',1),
(7,'answer 1',1),
(7,'answer 2',1),
(8,'answer 1',1),
(8,'answer 2',1),
(8,'answer 3',1),
(9,'answer 1',1),
(9,'answer 2',1),
(9,'answer 3',1),
(10,'answer 1',1),
(10,'answer 2',1),
(11,'answer 1',1),
(11,'answer 2',1),
(11,'answer 3',1),
(11,'answer 4',1),
(11,'answer 5',1),
(12,'answer 1',1),
(12,'answer 2',1),
(12,'answer 3',1),
(12,'answer 4',1),
(12,'answer 5',1),
(13,'answer 1',1),
(13,'answer 2',1),
(13,'answer 3',1),
(13,'answer 4',1),
(13,'answer 5',1),
(14,'answer 1',1),
(14,'answer 2',1),
(14,'answer 3',1),
(15,'answer 1',1),
(15,'answer 2',1),
(16,'answer 1',1),
(16,'answer 2',1),
(17,'answer 1',1),
(17,'answer 2',1);

insert into test_result(user_id, test_id, lat, lng, result) values 
(1,1,54.842904, 83.090842,100),
(1,1,54.857830, 83.111207,80),
(1,2,54.859945, 83.093102,91),
(2,3,54.842904, 83.090842,23),
(2,1,54.857830, 83.111207,54),
(3,2,54.859945, 83.093102,95);

