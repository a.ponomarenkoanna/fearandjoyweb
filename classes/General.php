<?php 
/*
This class provides handlers and information providers available for all users:
    /* get profile menu options
    /* get list of labels
    /* get list of test results
    /* see test list
    /* see world statistics
*/
require_once 'Connection.php';
class General extends Connection
{
    public function getOptionList($option){
        $queryResult = $this->getInformation("select name 
            from option_ inner join 
            role_option on (role_option.option_id  = option_.id) 
            where role_option.role_id = $option"); 
        return $queryResult;
    }

    public function getTestResultList(){
        $queryResult = $this->getInformation("select * from test_result");
        return $queryResult;
    }

    public function getLabelList(){
        $queryResult = $this->getInformation("select id, user_id,test_id,lat,lng, result from test_result");
        $jsonResult = json_encode($queryResult);
        return $jsonResult;
    }

    public function getWorldStatistics($dateFrom, $dateTo){
        $queryResult = $this->getTable("select year(tr.date_) as year, month(tr.date_) as month, day(tr.date_) as day, count(distinct tr.id) as number, avg(tr.result) as average 
        from test_result tr 
        group by month, year,day with rollup;");
        return $queryResult;
    }

    public function getPopTest(){
        $queryResult = $this->getInformation("select tr.test_id as id, t.name, t.description, count(tr.id) as number
        from test_result tr inner join test t on (tr.test_id = t.id)  
        group by tr.test_id 
        order by count(tr.id) desc limit 3");
        return $queryResult;
    }    
}
?>

