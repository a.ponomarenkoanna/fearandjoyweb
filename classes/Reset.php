<?php 
/*
This class provides handlers and information providers for changing password(action - forgot password):
    /* checking correctness of response
    /* changing of password
*/
class Reset extends Connection
{  
    public function getNameSurname($mail){
        $queryResult = $this->getInformation("select name, surname from user_ where user_.confirmed = true and user_.mail = \"$mail\"");
        if (count($queryResult)>0){
            return $queryResult[0];
        } else {
            return false;
        }
    }

    public function hashMatches($mail, $hash){
        $queryResult = $this->getNameSurname($mail);

        if (count($queryResult)>0){
   
            if(password_verify($mail.$queryResult['name'].$queryResult['surname'] , $hash)) {
                
                return true;
            }
            else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function resetPassword($mail, $password){ 
        try{
            $hash = password_hash($password, PASSWORD_BCRYPT);
            $str = "update user_ set password=\"$hash\" where mail=\"$mail\"";
            $queryResult = $this->getInformation($str);
            return true;
        } catch (Exception $ex){
            return false;
        }
       
    }  
    
} 
?>

