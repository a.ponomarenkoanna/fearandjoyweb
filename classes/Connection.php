<?php 

/*
    This class if for creating single 
    connection to database such as select
* stores all the information about db
* 
*/
class Connection{ 
    private static $host = 'localhost';
    private static $user = 'admin';
    private static $pass = 'password';
    private static $db = 'fearandjoy';
    private static $charset = 'utf8';
    private static $dbType = 'mysql';
    //private static $dsn = self::$dbType.":host=".self::$host.";dbname=".self::$db.";charset=".self::$charset;
    private static $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    
    protected function getInformation($query)
    {   
        $dsn = self::$dbType.":host=".self::$host.";dbname=".self::$db.";charset=".self::$charset;
        $pdo = new PDO($dsn, self::$user, self::$pass, self::$opt);

        $data = array(); 
        $queryResult = $pdo->query($query);
        while ($row = $queryResult->fetch())
        {
            array_push($data, $row);
        }

        return $data;    
    }

    protected function makeQuery($query)
    {   
        $dsn = self::$dbType.":host=".self::$host.";dbname=".self::$db.";charset=".self::$charset;
        $pdo = new PDO($dsn, self::$user, self::$pass, self::$opt);

        $data = $pdo->query($query)->fetchAll(PDO ::FETCH_UNIQUE);
        return $data;    
    }   
    
    protected function getTable($query)
    {
        $dsn = self::$dbType.":host=".self::$host.";dbname=".self::$db.";charset=".self::$charset;
        $pdo = new PDO($dsn, self::$user, self::$pass, self::$opt);

        $colData = $pdo->query($query);
        for ($i=0;$i<$colData->columnCount(); $i++){
            $columnNames[$colData->getColumnMeta($i)['name']] = $colData->getColumnMeta($i)['name'];
        }
        $data = array_merge(array($columnNames),$pdo->query($query)->fetchAll());
        return $data;    
    }
}

?>