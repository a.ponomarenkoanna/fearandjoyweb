<?php
/*
This class provides handlers and information providers for creating new users(registration):
    /* see if there is user with chosen email
    /* see if user needs to confirm his profile  
    /* creating new user
    /* confirmation of profile
*/
class Registration extends Connection{
    public function registry($name,$surname,$mail, $password){ 
        // first we check if mail is in base (there is a user with this mail)
        if(!$this->getNameSurname($mail)){
            // create user in base
            $hash = password_hash($password, PASSWORD_BCRYPT);
            $str = "insert into user_ (name,surname, mail,password) values
            (\"$name\",\"$surname\",\"$mail\", \"$hash\")";
            $queryResult = $this->getInformation($str);
            return true;
        } else {
            return false;
        }        
    }

    public function toBeConfirmed($mail){
        $queryResult = $this->getInformation("select confirmed from user_ where user_.confirmed = false and user_.mail = \"$mail\"");
        if (count($queryResult)>0){
            return true;
        } else {
            return false;
        }
    }

    public function confirm($mail){
        $queryResult = $this->getInformation("update user_ set confirmed = true where user_.mail = \"$mail\"");
        return true;
    }

    public function getNameSurname($mail){
        $queryResult = $this->getInformation("select name, surname from user_ where user_.confirmed = true and user_.mail = \"$mail\"");
        if (count($queryResult)>0){
            return $queryResult[0];
        } else {
            return false;
        }
    }

    public function hashMatches($mail, $hash){
        $queryResult = $this->getInformation("select name from user_ where user_.mail = \"$mail\"");

        if (count($queryResult)>0){
            $name = $queryResult[0]['name'];   
            if(password_verify($mail.$name , $hash)) {
                $this->confirm($mail);
                return true;
            }
            else {
                return false;
            }
        } else {
            return false;
        }
    }
    
}
?>