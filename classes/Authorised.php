<?php 
/*
This class provides handlers and information for content available only for authorised usual users:
    /* see and edit profile info
    /* take tests
    /* see his history
    /* see his labels
*/
class Authorised extends Connection
{
    public function getOptionList(){
        
        $queryResult = $this->getInformation("select name 
            from option_ inner join 
            role_option on (role_option.option_id  = option_.id) 
            where role_option.role_id = 1"); 
        return $queryResult;
    }

    public function getProfileInfo($user_id){
        $queryResult = $this->getInformation("select 
            u.id as id, 
            u.name as name, 
            u.surname as surname,
            u.mail as mail,
            u.confirmed as confirmed, 
            avg(tr.result) as average, 
            count(distinct tr.id) as count  
            from user_ u inner join test_result tr on (u.id = tr.user_id)
        where u.id =".$user_id); 
        return $queryResult[0];
    }

    public function getHistory($user_id){
        $queryResult = $this->getTable("select date_ as Date, lat as Latitude, lng as Longtitude, test_id as Test, result as Result from test_result where user_id = ".$user_id);
        return $queryResult;
    }

    public function getProfileLabels($user_id){
        $queryResult = $this->getInformation("select * from test_result where user_id =".$user_id);
        $jsonResult = json_encode($queryResult);
        return $jsonResult;
    }
}

?>

