<?php 
/*
This class provides handlers and information providers for passisng test:
    /* getting test list
    /* getting test content  
    /* calculating test result
    /* adding result
 */
class Tests extends Connection
{
    public function getTestList(){
        
        $queryResult = $this->getInformation("select id,name from test "); 
        return $queryResult;
    }

    public function getTestInformation($id){
        $queryResult = $this->getInformation("select * from test where id =".(string)$id);
        return $queryResult;
    }


    public function getAnswers($question_id){
        $queryResult = $this->getInformation("select id,answer, weight from answer where question_id =".$question_id);
        return $queryResult;
    }

    public function getQuestions($test_id){
        $queryResult = $this->getInformation("select id, question,mode_ from question 
        where (test_id =".$test_id.") order by id;");
        return $queryResult;
    }

    public function getTest($test_id){
        $queryResult = $this->getInformation("select q.id, q.question, a.answer 
        from question q right join answer a on (q.id = a.question_id)
        where (q.test_id =".$test_id.")order by id;"); 
        return $queryResult;
    }

    public function addResult($user_id,$test_id, $lat, $lng, $result){
        try{
            $date = explode('+',date('c'))[0];
            $qstr = "insert into test_result(user_id, test_id, lat, lng, date_, result) values 
                (".$user_id.",".$test_id.", ".$lat.", ".$lng.", \"".$date."\", ".$result.")";
            $queryResult = $this->getInformation($qstr); 
            return $queryResult;
        } catch (Exception $ex){
            return false;
        }
    }

    public function calculateResult($test, $answers){
        $sum = 0;
        $max = 0;
        
        $queryStr = "select q.id as question, sum(a.weight) as maxresult
        from answer a inner join question q on (a.question_id = q.id) 
                    inner join test t on (q.test_id = t.id)
        where t.id = $test and q.mode_ = (select id from test_mode where name = \"sum\")
        group by q.id
        union
        select q.id as question, max(a.weight)
        from answer a inner join question q on (a.question_id = q.id) 
                    inner join test t on (q.test_id = t.id)
        where t.id = $test and q.mode_ = (select id from test_mode where name = \"max\")
        group by q.id;";

        $maxResults = $this->getInformation($queryStr);
        $testMode = $this->getInformation("select test_mode.name from test inner join test_mode on (test.mode_ = test_mode.id) where test.id = $test")[0]['name'];
        
        foreach ($answers as $key => $value) {
            //calculate only answers
            if (is_numeric($key) ){
                $rowNum = array_search($key, array_column($maxResults, 'question'));
                $answerMax = $maxResults[$rowNum]['maxresult'];
                $answerResult = (is_array($value) ? array_sum($value) : $value)/ $answerMax; 
                
                $sum += $answerResult;
                $max = ($answerResult > $max) ? $answerResult: $max;
            }
        } 

        switch($testMode){
            case("sum"): 
                $result = $sum*100/count(array_column($maxResults, 'maxresult'));
                break;
            case("max"):
                $result = $max/max(array_column($maxResults, 'maxresult'))*100;
                break;
            default:
                $result = 0;
        }
        return $result;
    }
}
?>
