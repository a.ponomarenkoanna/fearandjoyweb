<?php 
require_once("autoload.php");


if(isset($_COOKIE['session_key']) ){

    $authorised =true;
    $conn= new Authorisation;
    $session_info = $conn->getSessionInfo($_COOKIE['session_key']);
    $role_id = $session_info[0]['role_id'];
    $id = $session_info[0]['user_id'];
    //refresh session
    $timeAlive = 50000;
    setcookie ("session_key", $_COOKIE['session_key'], time()+ $timeAlive);
} else {
$authorised = false;
$role_id = 0;
}

if (isset($_GET['action']) || isset($_POST['action'])){
    $action =(isset($_GET['action']))? $_GET['action'] : $_POST['action'];
} else {
$action = false;
}

if (isset($_GET['content']) || isset($_POST['content'])){
    $content =(isset($_POST['content']))? $_POST['content'] : $_GET['content'];
} else {
$content = false;
}

if (isset($_POST['test']) || isset($_GET['test'])){
    $test =isset($_POST['test']) ? $_POST['test'] : $_GET['test'];
} else {
$test = 0;
}

$router = new Router($authorised,$content,$role_id,$action,$test,$id);
$router->getRoute();
?>
