<?php
 
function __autoload($class)
{
    if (file_exists("../routes/$class.php")){
        require_once "../routes/$class.php";
    } else {
        require_once "../classes/$class.php";
    }
}
