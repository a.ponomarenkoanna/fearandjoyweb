<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Fear&Joy Profile confirmation</title>
  <link rel="stylesheet" href="view/styles/confirmed.css">
</head>
<body>
<div class="container">
    <div class="text">
        <?php 
        $hash = isset($_GET['hash']) ? $_GET['hash'] : false;
        $email = isset($_GET['email']) ? $_GET['email'] : false;
            if ($email && $hash) {
                $conn = new Registration;

                // is already confirmed?
                //is there such a user? 
                if ($conn->toBeConfirmed($email)){
                    if ($conn->hashMatches($email,$hash)){
                        echo "You have succesfully confirmed your profile";
                    } else {
                        echo "Have you used right link to confirm the email? Check it please.";
                    }
                } else {
                    echo "There's no such a user or you've alredy confirmed profile.";
                }
            } else {
                echo "Link is wrong. Please follow link from the email.";
            }
        ?>
        </div>
    <form action="#">
        <div class="data">
            <label>Go to main page for Fear&Joy</label>
        </div>        

        <div class="btn">
            <div class="inner"></div>
            <button type="submit">go</button>
        </div>
    </form>
</div>
</body>
</html>

