<html>
<head>
    <title>Fear and Joy Index World statistic</title>
    <link rel="stylesheet" href="view/styles/main.css">	   
    <link rel="stylesheet" href="view/styles/World statistics.css">	
</head>
<body>
<?php 
include "header.php";
?> 

<div class="block">
    <div></div> 
    <div class="content">
        <div name="WorldStatistics">
            <h2>World statistics</h2>

            <?php 
            $conn = new General;
            $result = $conn->getWorldStatistics(null, null);
            print "<p><table>
                <thead><tr></tr></thead>
                <tbody>";
            foreach($result as $a_row):
                print "<tr>";
                foreach ($a_row as $field)
                    print "<td>$field</td>";
                print "</tr>";
            endforeach;
            print "</tbody></table>";   
            ?>
        </div>

        <div name="testRating"> 
            <h2>Rating of tests</h2>
            <?php
            $result = $conn->getPopTest();
            foreach($result as $a_row):
                print "<div class=\"testInfo\">";
                print "Test № ".$a_row['id']." ".$a_row['name']." has been taken ".$a_row['number']." times.<br>" ;
                print $a_row['description'];
                print "<br> <a href=?content=getTest&test=".$a_row['id']."> Get test </a><br>"; 
                print "</div><br>";
            endforeach;
            ?>    
        </div> 
    </div>
    <div></div>
</div>
</body>
</html>
