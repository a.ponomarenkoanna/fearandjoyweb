<!-- Here's header menu.
Header menu can be full or "short"(abbreviated).
Full munu includes:
    date-selectors
    logo
    to-test-list button
    user-options
    login/logout buton
Abbreviated menu do not include date selectors, to-test-list button.-->    
    
<?php 
if (isset($_GET['content']) && $_GET['content']=="chooseTest"){
    $abbreviated = FALSE;
} else {
    $abbreviated = TRUE;
}

?>
<link rel="stylesheet" href="view/styles/login.css">

<div class="topNavigation">

<div class="left-align">
    <?php 
    if ($abbreviated){
    ?>
    <!-- date selectors for filtering labels on screen-->    
            <div class="dates">
                <!--<button name="fromDate" value="">From Date</button>
                <button name="toDate" value="">To Date</button>
                -->
            </div>
    <?php } ?>
    <?php 
    if ($abbreviated){
    ?>    
            <div class="location">
        
            </div>
    <?php } ?>
</div>
<div class="center-align">
    <!-- Logo -->
    <div class=logo>
        <a href="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
        <img  alt="Fear&JoyLogo" align-self="center" src="view/images/Logo.png" width="40%" height="70%">
        </a>
    </div>

    <!-- To test list button-->
    <?php 
    if ($abbreviated){
    ?>       
            <div class="toTests">
                <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"  method="POST">
                <button name="content" value="chooseTest" type="submit" class="chooseTest">
                    Get tests
                </button>
                </form>
            </div>
    <?php } ?>

</div>
<div class="right-align">
    <!-- User options -->
    <div class="dropdown">
        <button class="dropdown">Profile options
        </button>
        <div class="dropdown-content">
            <?php 
               $conn = new General;
               $result = $conn->getOptionList($role_id);
               foreach($result as $a_row)
                   foreach ($a_row as $field){ // $a_row – массив
                   print "\t<a href=\"?content=$field\">".$field."</a>\n"; 
                   }               
            ?>             
        </div>
    </div>   

    <!-- Login/logout buttons-->
    <div class="login-btn">
        <?php  
        include 'view/headerContent/Notifications.php';
        
        if ($authorised==true) 
            print("<a href=\"?action=Logout\" class=\"show-btn\">Logout</a>");
        else{
            print("<label for=\"show-login\" class=\"show-btn\">Login</label>");

            //including modules for authorisation/registration
            include 'view/headerContent/Login.php';
            include 'view/headerContent/Signup.php';
            
        ?>
        <!--radio "log-form" informs what additional windows for authorisation/registration is shown-->
        <div class="log-form">         
            <input type="radio" name="log-form" id="show-none"> 
        </div>
        <?php }?>     
    </div>
</div>
</div>
