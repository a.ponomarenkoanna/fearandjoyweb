<html> 
<head>
    <title>Fear and Joy Index Getting Started</title>
    <link rel="stylesheet" href="view/styles/main.css">	
    <link rel="stylesheet" href="view/styles/test.css">	
</head>
<body>
    
 <?php 
include "header.php";
 ?>   
 
<div class="block">
 <div></div>
<div id="chooseTest">

    <form action="<?php htmlentities($_SERVER['PHP_SELF']); ?>" method="POST"> 
    
        <?php
            print "<input type=\"hidden\" name=\"test\" value=$test><br>";
            $conn = new Tests;
            print "<h2>Test ".$test."</h2>";

            $test = $conn->getQuestions($test);
  
            foreach($test as $question){
                print $question['question']."<br>";
                $answers = $conn->getAnswers($question['id']);
                
                if ($question['mode_'] == 1){
                    for($i =0; $i < count($answers); $i++){
                        $answer = $answers[$i];
                        print "<input type=\"checkbox\" name=\"".$question['id']."[]\" value=\"".$answer['weight']."\">".$answer['answer']."<br>";
                    }
                } else {
                    for($i =0; $i < count($answers); $i++){
                        $answer = $answers[$i];
                        print "<input type=\"radio\" name=\"".$question['id']."\" value=\"".$answer['weight']."\">".$answer['answer']."<br>";
                    }
                }
                print "<br>";
            } 
            
        ?>
        <button name="content" value="getResult" >
            Get result
        </button>

    </form>
</div>
<div></div>
</div>

</body>
</html>
