ymaps.ready(init); 

function init(){
    var myMap = new ymaps.Map("map", {
        center: [54.84, 83.10],
        zoom: 10
    },{
        searchControlProvider: 'yandex#search'
    });

    
    mark = new ymaps.Placemark(myMap.getCenter(), {}, { draggable: 1 });
    //document.getElementById("inputplacemarkpos").innerText = mark.geometry.getCoordinates();
    document.getElementById("placemarkpos").innerText = mark.geometry.getCoordinates();
    let input = document.getElementById('inputplacemarkpos');
    input.value = mark.geometry.getCoordinates(); 

    myMap.geoObjects.add(mark);
    mark.events.add('drag', function(e) {
        document.getElementById("placemarkpos").innerHTML = mark.geometry.getCoordinates();
        input.value = mark.geometry.getCoordinates();
    });
}


                      
