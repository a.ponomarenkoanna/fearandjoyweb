var REGIONS_DATA = {
    region: {
        title: 'Регион',
        items: [{
            id: '001',
            title: 'Страны мира'
        }, {
            id: 'BY',
            title: 'Беларусь'
        }, {
            id: 'KZ',
            title: 'Казахстан'
        }, {
            id: 'RU',
            title: 'Россия'
        }, {
            id: 'TR',
            title: 'Турция'
        }, {
            id: 'UA',
            title: 'Украина'
        }]
    },
    quality: {
        title: 'Точность границ',
        items: [{
            id: '0',
            title: '0'
        }, {
            id: '1',
            title: '1'
        }, {
            id: '2',
            title: '2'
        }, {
            id: '3',
            title: '3'
        }]
    }
};

ymaps.ready(init);

function init() {
// Создадим собственный макет RegionControl.
    // Наследуем класс нашего контрола от ymaps.control.Button.
    RegionControl = ymaps.util.defineClass(function (parameters) {
        RegionControl.superclass.constructor.call(this, parameters);
    }, ymaps.control.Button, /** @lends ymaps.control.Button */{
        onAddToMap: function (map) {
            RegionControl.superclass.onAddToMap.call(this, map);
            this.setupStateMonitor();
            this.loadRegions(this.state.get('values'));
        },

        onRemoveFromMap: function (map) {
            map.geoObjects.remove(this.regions);
            this.clearStateMonitor();
            RegionControl.superclass.onRemoveFromMap.call(this, map);
        },

        setupStateMonitor: function () {
            this.stateMonitor = new ymaps.Monitor(this.state);
            this.stateMonitor.add('values', this.handleStateChange, this);
        },

        clearStateMonitor: function () {
            this.stateMonitor.removeAll();
        },

        handleStateChange: function (params) {
            this.loadRegions(params);
        },

        handleRegionsLoaded: function (res) {
            if(this.regions){
                map.geoObjects.remove(this.regions);
            }

            this.regions = new ymaps.ObjectManager();
            this.regions
                .add(res.features.map(function (feature) {
                    feature.id = feature.properties.iso3166;
                    feature.options = {
                        strokeColor: '#ffffff',
                        strokeOpacity: 0.4,
                        fillColor: '#90be6d',
                        fillOpacity: 0.8,
                        hintCloseTimeout: 0,
                        hintOpenTimeout: 0
                    };
                    return feature;
                }));
            map.geoObjects.add(this.regions);

            this.selectedRegionId = '';
            this.regions.events
                .add('mouseenter', function (e) {
                    var id = e.get('objectId');
                    this.regions.objects.setObjectOptions(id, {strokeWidth: 2});
                }, this)
                .add('mouseleave', function (e) {
                    var id = e.get('objectId');
                    if (this.selectedRegionId !== id) {
                        this.regions.objects.setObjectOptions(id, {strokeWidth: 1});
                    }
                }, this)
            this.getMap().setBounds(
                this.regions.getBounds(),
                {checkZoomRange: true}
            );
        },

        loadRegions: function (params) {
            this.disable();
            return ymaps.borders.load(params.region, params)
                .then(this.handleRegionsLoaded, this)
                .always(this.enable, this);
        }
    }),

    map = new ymaps.Map('map', {
        center: [50, 30],
        zoom: 3,
        controls: ['typeSelector']
    }, {
        typeSelectorSize: 'small'
    }),

    // Создадим экземпляр RegionControl.
    regionControl = new RegionControl({
        state: {
            enabled: true,
            values: {
                region: '001',
                quality: '1'
            }
        },
        data: {
            params: REGIONS_DATA
        },
        float: 'left',
        maxWidth: [300]
    });

// Добавим контрол на карту.
map.controls.add(regionControl);
// Узнавать о изменениях параметров RegionControl можно следующим образом.
regionControl.events.add('statechange', function (e) {
    console.log(e.get('target').get('values'));
});
}
