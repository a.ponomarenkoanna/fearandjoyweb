ymaps.ready(init);        
function init(){
    var myMap = new ymaps.Map("map", {
        center: [54.84, 83.10],
        zoom: 12
    },{
        searchControlProvider: 'yandex#search'
        }),
    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
    '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
    );

    var labelData = JSON.parse(document.getElementById('labelData').textContent);
    labelData.forEach(function(label){
        console.log(label);
        if (label.result > 80){
            imgName = 'very happy';
        } else if (label.result > 60){
            imgName = 'happy';
        } else if (label.result > 40){
            imgName = 'sick';
        } else if (label.result > 20){
            imgName = 'sad';
        } else {
            imgName = 'very sad';
        }
        myPlacemark = new ymaps.Placemark([label.lat, label.lng], {
            hintContent: 'Test N '+label.test_id+' with result '+ label.result,
            balloonContent: 'А эта — новогодняя'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#imageWithContent',
            // Своё изображение иконки метки.
            iconImageHref: 'view/images/'+imgName+'.png',
            // Размеры метки.
            iconImageSize: [26, 26],
 
            iconContentLayout: MyIconContentLayout
        });
    
        myMap.geoObjects 
        .add(myPlacemark);
    });

  }  




                      