<link rel="stylesheet" href="view/styles/notifications.css">

<div class="notification">
    <input type="radio" name="notification" id="notification-none">   
</div>

<div class="not-available">
    <input type="radio" name="notification" id="show-not-available" <?php if($action=="notAvailable") echo "checked"; ?>>
    <div class="container">
    <label for="notification-none" class="close-btn" title="close">Close</label>
        <div class="text">
            This action is not available for unauthorised users.
        </div>
        <form>
        <div class="btn">
            <div class="inner"></div>
            <label for="show-login" class=button>To login</label>
        </div>
        </form>
    </div>
</div>

<div class="result-submitted">
    <input type="radio" name="notification" id="show-result-submitted" <?php if($action=="resultSubmitted") echo "checked"; ?>>
    <div class="container">
    <label for="notification-none" class="close-btn" title="close">Close</label>
        <div class="text">
            Thank you! Your result has been submitted!
        </div>
    </div>
</div>

<div class="result-not-submitted">
    <input type="radio" name="notification" id="show-result-not-submitted" <?php if($action=="resultNotSubmitted") echo "checked"; ?>>
    <div class="container">
    <label for="notification-none" class="close-btn" title="close">Close</label>
        <div class="text">
            Something went wrong. Your result has not been submitted!
        </div>
    </div>
</div>

<div class="forgot-password-sent">
    <input type="radio" name="notification" id="show-forgot-sent" <?php if($action=="forgot-sent") echo "checked"; ?>>
    <div class="container">
        <label for="notification-none" class="close-btn" title="close">Close</label>
        <div class="text">
            Email for resetting your password was sent.  
        </div>   
    </div>
</div>

<div class="resetted-password">
    <input type="radio" name="notification" id="show-resetted-password" <?php if($action=="resetted-password") echo "checked"; ?>>
    <div class="container">
        <label for="notification-none" class="close-btn" title="close">Close</label>
        <div class="text">
            You have successfully reseted you password!  
        </div>   
    </div>
</div>

<div class="not-resetted-password">
    <input type="radio" name="log-form" id="show-not-resetted-password" <?php if($action=="not-resetted-password") echo "checked"; ?>>
    <div class="container">
        <label for="show-none" class="close-btn" title="close">Close</label>
        <div class="text">
            Something went wrong. Your password was not resetted.  
        </div>   
    </div>
</div>

<div class="login-wrong">
    <input type="radio" name="log-form" id="show-login-wrong" <?php if($action=="wrong-login") echo "checked"; ?>>
    <div class="container">
    <label for="show-none" class="close-btn" title="close">Close</label>
        <div class="text">
            Email or password is wrong! Try again!
        </div>
        <br>
        <form>
            <div class="btn">
                <div class="inner"></div>
                <label for="show-login" class=button>To login</label>
            </div>
        </form>
    </div>
</div>

<div class="confirmation">
    <input type="radio" name="log-form" id="show-signuped" <?php if($action=="signuped") echo "checked"; ?>>
    <div class="container">
    <label for="show-none" class="close-btn" title="close">Close</label>
        <div class="text">
            Thank you for registration!
            Please confirm your profile using link from your email. 
        </div>
        <form>
            <div class="btn">
                <div class="inner"></div>
                <label for="show-login" class=button>To login</label>
            </div>
        </form>
    </div>
</div>

<div class="mail-occupied">
    <input type="radio" name="log-form" id="show-mail-occupied" <?php if($action=="mail-occupied") echo "checked"; ?>>
    <div class="container">
    <label for="show-none" class="close-btn" title="close">Close</label>
        <div class="text">
            There is a profile connected to this e-mail. Please choose another e-mail or reset password to existing profile. 
        </div>
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"  method="POST">
            <div class="btn">
                <div class="inner"></div>
                <label for="show-signup" class=button>To signup</label>
            </div>
            <div class="btn">
                <div class="inner"></div>
                <label for="show-forgot-password" class=button>forgot password</label>
            </div>
        </form>
    </div>
</div>
