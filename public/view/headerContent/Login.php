<div class="login-form">
    <input type="radio" name="log-form" id="show-login">
    <div class="container">
        <label for="show-none" class="close-btn">Close</label>
        <div class="text">
        Login Form
        </div>
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"  method="POST">
            <div class="data">
                <label>Email</label>
                <input type="text" required name="email">
            </div>
            <div class="data">
                <label>Password</label>
                <input type="password" required name="password" >
            </div>
            <div class="forgot-pass">
                <label for="show-forgot-password">Forgot Password?</label>
            </div>
            <div class="btn">
                <div class="inner"></div>
                <button type="submit" name="action" value="Login">login</button>
            </div>
            <div class="signup-link">
                Not a member?
                <label for="show-signup" class="show-btn">Signup</label>
            </div>
        </form>
    </div>
   
</div>

<div class="forgot-password">
    <input type="radio" name="log-form" id="show-forgot-password" <?php if($action=="forgot-password") echo "checked"; ?>>
    <div class="container">
    <label for="show-none" class="close-btn" title="close">Close</label>
        <div class="text">
            Forgot password
        </div>
        <br>
        <div>
            If you have Fear&Joy account we will send link to change password!
        </div>
        <div>
            *resetting password is available only for confirmed profiles
        </div>
        <br>
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"  method="POST">
    
            <div class="data">
                <label>Email</label>
                <input type="text" required name="email">
            </div>
        
            <div class="btn">
                <div class="inner"></div>             
                <button type="submit" name="action" value="ForgotPassword">Get link</button>            
            </div>
        </form>
    </div>
</div>

