<div class="signup-form">
    <input type="radio" name="log-form" id="show-signup">
    <div class="container">
        <label for="show-none" class="close-btn" title="close">Close</label>
        <div class="text">
        Register Form
        </div>
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"  method="POST">
            <div class="data">
                <label>Name*</label>
                <input type="text" required name="name" >
            </div>
            <div class="data">
                <label>Surname</label>
                <input type="surname" name="surname" >
            </div>
            <div class="data">
                <label>Email*</label>
                <input type="text" required name="email">
            </div>
            <div class="data">
                <label>Password*</label>
                <input type="password" required name="password" >
            </div>
            <div class="forgot-pass">
                * - required field
            </div>
            <div class="btn">
                <div class="inner"></div>
                <button name="action" value="Signup" >
                    signup
                </button>
            </div>
            
        </form>
    </div>
</div>
