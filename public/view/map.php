<html>
<head>
    <title>Fear and Joy Index Getting Started</title>
    <link rel="stylesheet" href="view/styles/main.css">	   
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">
    </script>
    <script id="labelData" type="application/json">
    <?php
        $conn = new General;
        $result = $conn->getLabelList();
        
        print $result
    ?>
    </script>
    <script type="text/javascript" src="view/js/mapContent.js"></script>
</head>
<body>

<?php include "header.php"?>

<div id="map" style="width: 100%; height: 90%"></div>

</body>
</html>


