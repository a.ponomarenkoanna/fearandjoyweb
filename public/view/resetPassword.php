<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Fear&Joy Profile confirmation</title>
  <link rel="stylesheet" href="view/styles/confirmed.css">
</head>
<body>
<div class="container">
    <div class="text">
        <?php 
        $hash = isset($_GET['hash']) ? $_GET['hash'] : false;
        $mail = isset($_GET['email']) ? $_GET['email'] : false;
        if ($mail && $hash) {
            
            $reset = new Reset;
            //is there such a user? 
            //are input values correct? 
            if ($reset->hashMatches($mail,$hash)){?>
                Please enter new password to your profile
                <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"  method="POST">
                    <div class="data">
                        <label>New password</label>
                        <input type="password" required name="password" >
                    </div> 
                    <?php
                    print("<input type=\"hidden\" name=\"mail\" value=\"$mail\">");
                    
                    ?>                   
                    <div class="btn">
                        <div class="inner"></div>
                        <button type="submit" name="action" value="passwordReseted">reset&login</button>
                    </div>
                </form>
            <?php
            } else {
                echo "Have you used right link to confirm the email? Check it please.";
            }
                        
        } else {
            echo "Link is wrong. Please follow link from the email.";
        }
        ?>
    </div>  
</div>
</body>
</html>

