<html>
<head>
    <title>My history</title>
    <link rel="stylesheet" href="view/styles/main.css">
    <link rel="stylesheet" href="view/styles/My history.css">	   
</head>
<body>
<?php 
include "header.php";
?>

<div class = "block">
    <div></div>
    <div>
        <h2>You can see your test history:</h2>
        <?php 
            $conn = new Authorised;
            $result = $conn->getHistory($id);
            print "<p><table>
                <thead><tr></tr></thead>
                <tbody>";
            foreach($result as $a_row):
                print "<tr>";
                foreach ($a_row as $field)
                    print "<td>$field</td>";
                print "</tr>";
            endforeach;
            print "</tbody></table>";
        ?>
    </div>
    <div></div>
</div>
</div>
</div>

</body>
</html>
