<html>
<head>
    <title>My labels</title>
    <link rel="stylesheet" href="view/styles/main.css">	
    <link rel="stylesheet" href="view/styles/My labels.css">	   
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">
    </script>
    <script id="labelData" type="application/json">
    <?php
        $conn = new Authorised;
        $result = $conn->getProfileLabels($id);
        
        print $result
    ?>
    </script>
    <script type="text/javascript" src="view/js/mapContent.js"></script>
</head>
<body>
<?php 
include "header.php";
?>      
<div class = "block">
    <div></div>
    <div class = "labelMap">
        <h2>You can see your labels here:</h2>
        <div id="map" style="width: 100%; height: 100%"></div> 
    </div>
    <div></div>
</div>

</body>
</html>
