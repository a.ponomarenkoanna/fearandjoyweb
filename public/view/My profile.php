<html>
<head>
    <title>Fear and Joy Index Getting Started</title>
    <link rel="stylesheet" href="view/styles/main.css">	   
    <link rel="stylesheet" href="view/styles/My profile.css">	
</head>
<body>
<?php 
include "header.php";
?>      
 
<div class="block">
	<div></div>
  <?php 
    $conn = new Authorised;
    $result = $conn->getProfileInfo($id); 
  ?>
  
  <div class="avatar-flip">
    <img src="view/images/very happy.png" height="150" width="150">
    <img src="view/images/<?php print( ($result['confirmed'] == true)? "very sad.png" : "Logo.png"); ?>"
     height="150" width="150">
  </div>
  <div class="profile-info"> 
  <?php 
    print("<h2>".$result['name']." ".$result['surname']."</h2>");
    print("<h4>".(($result['confirmed'] == true)? "Confirmed user" : "Not confirmed user")."</h4>");
    print("<h4>".$result['mail']."</h4>");
    print("<p>You have taken ".$result['count']." tests with  average result ".$result['average']."</p>");
  ?>
	</div>
  <div></div>
</div>

</body>
</html>
