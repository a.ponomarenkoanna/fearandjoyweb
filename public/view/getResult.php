<html>  
<head>
    <title>Submit your result</title>
    <link rel="stylesheet" href="view/styles/main.css">
    <link rel="stylesheet" href="view/styles/test.css">
    
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript" src="view/js/resultMap.js"></script>
</head>
<body>
    
<?php 
include "header.php";
?>  
<div class="block">
    <div></div> 
    <div id="chooseTest">
        <form action="<?php htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">  
            <?php           
                $conn = new Tests;
                $answers = $_POST; 
                print("<input type =\"hidden\" name=\"test\" value=$test></input>");
                $result = $conn->calculateResult($test,$answers);
                print "Your result is $result"; 
                $test = $conn->getQuestions($test);   
                print("<input type =\"hidden\" name=\"result\" value=$result></input>");
                
            ?>
        
            Choose place:
            <input type ="hidden" name="content" value="submitResult"></input>
            <input type ="hidden" id="inputplacemarkpos" name="placemarkpos"></input> 
            <div id="placemarkpos"></div>
            
            <div id="map" style="width: 100%; height: 100%"></div>

            <button value="submit">
                Submit my result
            </button>   
        </form>

        <div>
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
            <button type="submit">
                To main page
            </button>
        </form>
        </div>
    </div>
    <div></div>
</div>
</body>
</html>
