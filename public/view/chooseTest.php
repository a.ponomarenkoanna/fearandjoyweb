<?php namespace Test;
?>
<html>
    
<head>
    <title>Fear and Joy Index Choosing test</title>
    <link rel="stylesheet" href="view/styles/main.css">	
    <link rel="stylesheet" href="view/styles/test.css">	

</head>
<body>   
 <?php 
include "header.php";
 ?>   

<div class="block">
<div></div>
<div class="chooseTest">
    <form action="<?php htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">

        <select name="test">
            <option disabled>Choose Test</option> 
            <?php 
                use Tests;
                $conn = new Tests;
                $data = $conn->getTestList();
                foreach ($data as $row )
                {
                    print "<option id=\"test\" value=\"".$row['id']."\">".$row['name']."</option>/n";
                }
            ?> 
        </select>

        
        <?php
        $NUM_TEST_PREVIEW = 3;
            for($i=1;$i<(1+$NUM_TEST_PREVIEW);$i++){
                $testInfo = $conn->getTestInformation($i);
                foreach($testInfo as $row){
                    print "<div class=\"testInfo\">";
                    foreach($row as $field)
                        print $field."<br>";
                    print "</div><br><br>\n";
                }
            }
        ?>
        <button type="submit" name="content" value="getTest">
            Choose
        </button>
    </form>
</div>
<div>
</div>
</div>

</body>
</html>
